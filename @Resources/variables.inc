[Variables]

; USER-DEFINED VARIABLES
tempWarm=60
tempHot=80
spaceWarn=10

; fonts
fontTemp=Exo 2 Light
fontLabel=Poppins Light

; colors
colorText=200,200,200,255
colorCold=71,165,31,255
colorWarm=226,203,27,255
colorHot=229,60,41,255
colorBG=25,25,25,255

; arc definitions
startAngle=150
rotationAngle=240