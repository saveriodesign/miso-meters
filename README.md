# Miso Meters v0.1.2

This skin fills a gap I felt was missing; minimal system monitoring circles that
aren't obnoxiously huge, overly rich in information, or CPU-nomming histograms.

## Proccesor Monitors

![Processor Monitors](/images/cpugpu.png)

A simple visualization of CPU and GPU temperature (the number) and usage (the arc
around the number). The usage arc changes color as the temperature
rises to draw attention as your hardware heats up.

### Setting the color levels

By default, the color changes from green to yellow at 60 &deg;C and to red at 80
&deg;C. If you'd like to change these settings

1. Open "~/Documents/Rainmeter/Skins/MisoMeters/@Resources/variables.inc"
1. Change the values of "tempWarm" and "tempHot" on Lines 4 and 5

## Storage Monitors

![Storage Monitors](/images/drives.png)

Diplays approximate free disk space and visualizes fullness. When the drive reaches
90% capacity, the arc turns red. To change the drive space warning,

1. Open "~/Documents/Rainmeter/Skins/MisoMeters/@Resources/variables.inc"
1. Change the values of "spaceWarn" Lines 6

### Setting drive letters

1. Right-click the skin
1. Select "Edit Skin"
1. Change the variable "drive" on Line 17 to the appropriate letter

> But, Mike, I have more than 4 disk drives!

Then you have enough savvy to copy + paste the skin folder and make infinite
disk monitors without a walkthrough.

## Requirements

[Rainmeter](https://www.rainmeter.net/)  
[HWiNFO](https://www.hwinfo.com/) and [Plugin](https://www.hwinfo.com/forum/Thread-Rainmeter-plug-in-for-HWiNFO-3-2)
